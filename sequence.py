from colors import COLORS

from tile import Tile
class SingleSequence:
    def __init__(self, number, color):
        self.type = "single"
        self.number = number
        self.colorSet = {color}

        self.color = color
        self.startNumber = number
        self.endNumber = number
    def isValid(self):
        return False

class GroupSequence:
    def __init__(self, number, colorSet):
        self.type = "group"
        self.number = number
        self.colorSet = colorSet
    def isValid(self):
        return len(self.colorSet) >= 3
    def __str__(self) -> str:
        return "Group of " + str(self.number) + "s with colors " + str(self.colorSet)
    def getTiles(self):
        return list(map(lambda color: Tile(color + str(self.number)), list(self.colorSet)))

class RunSequence:
    def __init__(self, startNumber, endNumber, color):
        self.type = "run"
        self.startNumber = startNumber
        self.endNumber = endNumber
        self.color = color
    def isValid(self):
        return self.endNumber - self.startNumber >= 2 
    def __str__(self) -> str:
        return "Run from " + str(self.startNumber) + " to " + str(self.endNumber) + " with color " + str(self.color)
    def getTiles(self):
        return list(map(lambda number: Tile(self.color + str(number)), list(range(self.startNumber, self.endNumber + 1))))

def SequenceBuilder(string: str):
    if string[0] in COLORS:
        color = string[0]
        start, end = string[1:].split('-')
        startNumber = int(start)
        endNumber = int(end)
        return RunSequence(startNumber,endNumber,color)
    else:
        endIndex = 1
        if string[1] not in COLORS:
            endIndex = 2
        number = int(string[0:endIndex])
        colorSet = set()
        for color in string[endIndex:]:
            colorSet.add(color)
        return GroupSequence(number, colorSet)

def tryMakeSequence(tile, sequence = None):
    if sequence is None:
        return SingleSequence(tile.number, tile.color)
    if sequence.type == "group" or sequence.type == "single":
        if sequence.number == tile.number:
            if tile.color in sequence.colorSet:
                return False
            newSet = {tile.color}
            newSet.update(sequence.colorSet)
            return GroupSequence(tile.number, newSet)
    if sequence.type == "run" or sequence.type == "single":
        if tile.color == sequence.color:
            if tile.number == sequence.endNumber + 1:
                return RunSequence(sequence.startNumber, sequence.endNumber + 1, sequence.color)
            if tile.number == sequence.startNumber - 1:
                return RunSequence(sequence.startNumber - 1, sequence.endNumber, sequence.color)
    return False