from colors import COLORS

class Tile:
    def __init__(self, string):
        if len(string) < 2:
            raise ValueError("Invalid input, tiles are made up of at least 2 characters")
        color = string[0]
        value = string[1:]
        if color not in COLORS:
            raise ValueError("Invalid color, valid values are " + ', '.join(COLORS))
        try:
            number = int(value)
        except ValueError:
            raise ValueError("Invalid numeral")
        if number < 1:
            raise ValueError("Number must be at least 1")
        if number > 13:
            raise ValueError("Number must be at most 13")
        self.color = color
        self.number = number
    