#!/usr/bin/env python3

import sys
from sequence import SequenceBuilder, tryMakeSequence

from tile import Tile
import itertools

#TODO: Add Joker functionality

def usage():
    print("USAGE: main.py [board tiles] [your tiles]")
    print("where tiles are a list of tiles, separated by commas, in the format [color][number]")
    print("where color is one of R, B, Y, I (red, black, yellow, indigo (blue))")
    print("and number is anywhere from 1-13")
    sys.exit(1)

def convert(string):
    return Tile(string)

def createSequences(tiles: list[Tile], reserveTiles: list[Tile], currentSequence=None, sequences=[], touchedReserve=False):
    if currentSequence is None:
        if len(tiles) > 0:
            return createSequences(tiles[1:], reserveTiles, tryMakeSequence(tiles[0]), sequences, touchedReserve)
        elif len(reserveTiles) > 0:
            for tile in reserveTiles:
                possibleSequence = tryMakeSequence(tile, currentSequence)
                if possibleSequence:
                    result = createSequences(tiles, list(filter(lambda t: t != tile, reserveTiles)), possibleSequence, sequences, True)
                    if result:
                        return result
            return False
    if len(tiles) == 0:
        for tile in reserveTiles:
            possibleSequence = tryMakeSequence(tile, currentSequence)
            if possibleSequence:
                result = createSequences(tiles, list(filter(lambda t: t != tile, reserveTiles)), possibleSequence, sequences, True)
                if result:
                    return result
        if currentSequence is not None and not currentSequence.isValid():
            return False
        if not touchedReserve:
            if currentSequence.isValid():
                newsequences = []
                for sequence in sequences:
                    newsequences.append(sequence)
                newsequences.append(currentSequence)
                return createSequences(tiles, reserveTiles, None, newsequences, touchedReserve)
            return False
        else:
            newsequences = []
            for sequence in sequences:
                newsequences.append(sequence)
            newsequences.append(currentSequence)
            return newsequences
    
    for tile in tiles:
        possibleSequence = tryMakeSequence(tile, currentSequence)
        if possibleSequence:
            result = createSequences(list(filter(lambda t: t != tile, tiles)), reserveTiles, possibleSequence, sequences, touchedReserve)
            if result:
                return result
    for tile in reserveTiles:
        possibleSequence = tryMakeSequence(tile, currentSequence)
        if possibleSequence:
            result = createSequences(tiles, list(filter(lambda t: t != tile, reserveTiles)), possibleSequence, sequences, True)
            if result:
                return result
    if currentSequence.isValid():
        newsequences = []
        for sequence in sequences:
            newsequences.append(sequence)
        newsequences.append(currentSequence)
        return createSequences(tiles, reserveTiles, None, newsequences, touchedReserve)
    
    return False

def canCreateSequence(tiles: list[Tile], tile:Tile):
    return canFinishSequence(tiles, tryMakeSequence(tile))
def canFinishSequence(tiles: list[Tile], currentSequence):
    for tile in tiles:
        possibleSequence = tryMakeSequence(tile, currentSequence)
        if possibleSequence:
            if possibleSequence.isValid():
                return True
            result = canFinishSequence(list(filter(lambda t: t != tile, tiles)), possibleSequence)
            if result:
                return result
    return False

if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage()
    boardSequences = list(map(lambda s: SequenceBuilder(s), sys.argv[1].split(",")))
    boardTiles = []
    for sequence in boardSequences:
        boardTiles.extend(sequence.getTiles())
    yourTiles = list(map(lambda s: Tile(s), sys.argv[2].split(",")))
    actualYourTiles = []

    for tile in yourTiles:
        newtiles = []
        for boardTile in boardTiles:
            newtiles.append(boardTile)
        newtiles.extend(list(filter(lambda t: t != tile, yourTiles)))
        if canCreateSequence(newtiles, tile):
            actualYourTiles.append(tile)
    print("Only " + str(len(actualYourTiles)) + " tiles from your hand are feasible")

    sequences = createSequences(boardTiles, yourTiles)
    if sequences:
        print()
        print("---SOLUTION---")
        print()
        for sequence in sequences:
            print(sequence)
        sys.exit(1)

    # for i in range(len(actualYourTiles), 0, -1):
    #     print("Trying to play " + str(i) + " tiles from your hand")
    #     combinations = list(itertools.combinations(actualYourTiles, i))
    #     j = 0
    #     for subset in combinations:
    #         j += 1
    #         print(".", end="", flush=True)
    #         newtiles = []
    #         for tile in boardTiles:
    #             newtiles.append(tile)
    #         for tile in list(subset):
    #             newtiles.append(tile)
    #         sequences = createSequences(newtiles)
            
    #     print()
    print("No possible moves found :(")